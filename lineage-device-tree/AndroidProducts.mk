#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_davinciin.mk

COMMON_LUNCH_CHOICES := \
    lineage_davinciin-user \
    lineage_davinciin-userdebug \
    lineage_davinciin-eng
